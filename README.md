OBSERVATIONIS
Site Web traitant des systèmes de surveillance

Philippe Baus - Elias Hietanen - Johan Rieu Patey - Edwin van De Plasse

Groupe S1B
Groupe de PTUT : 1

Respect des contraintes : 
Images : des images sont présentes sur toutes les pages.
Images cliquables : présentent en plusieurs endroit sur le site, notament sur la page d'accueil
Vidéo : une vidéo est présente sur la page numerique.html ATTENTION : parfois, sur Chrome, la vidéo refuse de s'afficher.
Ancres : des ancres ont été utilisées pour réaliser le menu de droite présent sur les pagesde contenu
Liens vers d'autres sites : par exemple, le logo de l'iut présent sur la paged'acceuil envoie vers le site de l'iut
Tableau : un tableau est présent sur la page physique.html

Nom de la page d'accueil : index.html
HTML et CSS : séparés
Nombre de pages sur le site : 6
Le nom des auteurs et le logo apparaissent
Les liens sont relatifs (exemple: ..\Ressources\Images\logo.png)
Nombre de lignes de code : environ 750
Responsive design : nous avons fait le maximum avec les connaissances que nous avions.
Sources : voir biblio.html
lien du git : https://framagit.org/pgu/ptut-systemes_de_surveillance

Fonctionalités principales du site :
	- Les icones de la page d'accueil sont animés 
	- Le bandeau du haut et le menu de droite restent afficher en permanence à l'écran, pour rester accessibles
	- Les liens vers des sites externes s'ouvrent dans un nouvel onglet
	- Le système d'ancre fonctionne malgrès le menu du haut "fixed"
	- Le menu du haut et de droite on des effets lorsqu'on passe la souri dessus
	- Le menu du haut indique, grâce à un changement de couleur du bouton, sur quelle pas on se trouve
	- A partir d'une certaine taille d'écran, le menu de droite disparaît pour permettre une lecture en pleine page du site sur les petits écrans